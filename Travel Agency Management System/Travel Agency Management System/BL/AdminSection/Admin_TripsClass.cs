﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Travel_Agency_Management_System.BL.AdminSection
{
    public class Admin_TripsClass
    {
        private string packageName;
        private int package_price;
        private string departure;
        private int no_of_days;
        private string destination;
        private System.Drawing.Image pic;
      

        public Admin_TripsClass (string p_name, int p_price, string departure, int noOfdAYS , string destination,Image pict)
        {
            this.packageName = p_name;
            this.package_price = p_price;
            this.Departure=departure;
            this.No_of_days = noOfdAYS;
            this.Destination = destination;
            this.Pic = pict;
        }

        public string PackageName { get => packageName; set => packageName = value; }
        public int Package_price { get => package_price; set => package_price = value; }
       
        public int No_of_days { get => no_of_days; set => no_of_days = value; }
        public Image Pic { get => pic; set => pic = value; }
        public string Destination { get => destination; set => destination = value; }
        public string Departure { get => departure; set => departure = value; }
    }
}
