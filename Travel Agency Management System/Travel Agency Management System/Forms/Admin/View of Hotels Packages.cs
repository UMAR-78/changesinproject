﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travel_Agency_Management_System.Forms.Admin
{
    public partial class View_of_Hotels_Packages : UserControl
    {
        private static View_of_Hotels_Packages _instance;
        public static View_of_Hotels_Packages Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new View_of_Hotels_Packages();
                }
                return _instance;
            }
        }
        private View_of_Hotels_Packages()
        {
            InitializeComponent();
        }

        private void View_of_Hotels_Packages_Load(object sender, EventArgs e)
        {
            /*var con = Configuration.getInstance().getConnection();
            using (con)
            {
                  
                  SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM HotelPackageView", con);
                  DataTable dt = new DataTable();
                  da.Fill(dt);
                  dataGridView1.DataSource = dt;
            }*/
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM HotelPackageView", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
