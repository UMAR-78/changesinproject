﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Travel_Agency_Management_System.Forms.Admin
{
    public partial class ucReports : UserControl
    {
        private static ucReports _instance;
        public static ucReports Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ucReports();
                }
                return _instance;
            }
        }

        public void RefreshUI()
        {

        }
        public ucReports()
        {
            InitializeComponent();
        }
        private DataTable GetDataFromDatabase()
        {
            var con = Configuration.getInstance().getConnection();
            using (con)
            {
                
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM HotelPackageView", con);
                DataTable dt = new DataTable();
                sqlDa.Fill(dt);
               
                return dt;
            }
        }

        private void roundButton1_Click(object sender, EventArgs e)
        {
            DataTable dt = GetDataFromDatabase();

            // Create a new PDF document
            Document doc = new Document();
            PdfWriter.GetInstance(doc, new FileStream("Report.pdf", FileMode.Create));
            doc.Open();

            // Add a table with headers
            PdfPTable table = new PdfPTable(5);
            table.AddCell("HotelName");
            table.AddCell("Price ");
            table.AddCell("Area");
            table.AddCell("FeedBack");
            table.AddCell("Package Name");
            table.AddCell("Package Price");
            table.AddCell("Departure");
            table.AddCell("NoofDays");
            table.AddCell("Destination");

            // Add data rows to the table
            foreach (DataRow row in dt.Rows)
            {
                string hotelname = row["HotelName"].ToString();
                int price = Convert.ToInt32(row["PerPrice"]);
                string Area = row["Area"].ToString();
                string feedback = row["Feedback"].ToString();
                string Packagename = row["PackageName"].ToString();
                int packageprice = Convert.ToInt32(row["PackagePrice"]);
                string departure = row["Departure"].ToString();
                int noofdays = Convert.ToInt32(row["NoofDays"]);
                string destination = row["Destination"].ToString();

               

                table.AddCell(hotelname);
                table.AddCell(price.ToString());
                table.AddCell(Area);
                table.AddCell(feedback);
                table.AddCell(Packagename);
                table.AddCell(packageprice.ToString());
                table.AddCell(departure);
                table.AddCell(noofdays.ToString());
                table.AddCell(destination);


            }





            // Add the table to the document
            doc.Add(table);

            // Close the document
            doc.Close();

            MessageBox.Show("PDF report generated successfully!");
        }
    }
}
