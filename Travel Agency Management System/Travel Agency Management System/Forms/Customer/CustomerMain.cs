﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.Properties;
using Travel_Agency_Management_System.Utilities;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class CustomerMain : Form
    {
        
        private static CustomerMain _instance;
        public static CustomerMain Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CustomerMain();
                }
                return _instance;
            }
        }

        List<RoundButton> rbtnHoverTrue1 = new List<RoundButton>();
        List<RoundButton> rbtnHoverFalse = new List<RoundButton>();
        public List<RoundButton> RbtnHoverTrue1 { get => rbtnHoverTrue1; set => rbtnHoverTrue1 = value; }
        public List<RoundButton> RbtnHoverFalse { get => rbtnHoverFalse; set => rbtnHoverFalse = value; }

        private CustomerMain()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (RbtnHoverTrue1.Count < 1)
            {
                timer1.Stop();
            }
            else
            {
                for (int i = 0; i < RbtnHoverTrue1.Count; i++)
                {
                    if (RbtnHoverTrue1[i].Size.Height < 36)
                    {
                        RbtnHoverTrue1[i].Size = new Size(RbtnHoverTrue1[i].Size.Width + 1, RbtnHoverTrue1[i].Size.Height + 1);
                    }
                    else
                    {
                        RbtnHoverFalse.Add(RbtnHoverTrue1[i]);
                        RbtnHoverTrue1.Remove(RbtnHoverTrue1[i]);
                    }
                }
            }

            if (RbtnHoverTrue1.Count == 0)
            {
                timer1.Stop();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (RbtnHoverFalse.Count < 1)
            {
                timer2.Stop();
            }
            else
            {
                for (int i = 0; i < RbtnHoverFalse.Count; i++)
                {
                    if (RbtnHoverFalse[i].Size.Height > 30)
                    {
                        RbtnHoverFalse[i].Size = new Size(RbtnHoverFalse[i].Size.Width - 1, RbtnHoverFalse[i].Size.Height - 1);
                    }
                    else
                    {
                        RbtnHoverFalse.Remove(RbtnHoverFalse[i]);
                    }
                }
            }

            if (RbtnHoverFalse.Count == 0)
            {
                timer2.Stop();
            }
        }


        private void rbtnDashboard_MouseEnter(object sender, EventArgs e)
        {

            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtnDashboard_MouseLeave(object sender, EventArgs e)
        {

            timer2.Start();
        }

        private void rbtn_MyTickets_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_MyTickets_MouseLeave(object sender, EventArgs e)
        {

            timer2.Start();
        }

        private void rbtn_Bookings_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Bookings_MouseLeave(object sender, EventArgs e)
        {

            timer2.Start();
        }

        private void rbtn_Payments_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Payments_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void rbtn_Review_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Review_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void rbtn_Exit_MouseEnter(object sender, EventArgs e)
        {
            timer2.Start();
            RoundButton button = (RoundButton)sender;
            if (RbtnHoverFalse.Contains(button))
            {
                RbtnHoverFalse.Remove(button);
            }
            button.Size = new Size(147, 30);
            RbtnHoverTrue1.Add(button);
            if (!timer1.Enabled)
            {
                timer1.Start();
            }
        }

        private void rbtn_Exit_MouseLeave(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void rbtnDashboard_Click(object sender, EventArgs e) => Navigate.ToDashboard();

        private void rbtn_MyTickets_Click(object sender, EventArgs e) => Navigate.ToMyTickets();

        private void rbtn_Bookings_Click(object sender, EventArgs e) => Navigate.ToBookings();

        private void rbtn_Payments_Click(object sender, EventArgs e) => Navigate.ToCustomerPayments();

        private void rbtn_Review_Click(object sender, EventArgs e) => Navigate.ToReview();

        private void rbtn_Exit_Click(object sender, EventArgs e)
        {
            Login newForm = new Login();
            newForm.Show();
            this.Hide();
        }
    }
}
