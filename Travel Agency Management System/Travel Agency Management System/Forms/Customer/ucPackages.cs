﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travel_Agency_Management_System.BL.AdminSection;
using Travel_Agency_Management_System.Utilities;

namespace Travel_Agency_Management_System.Forms.Customer
{
    public partial class ucPackages : UserControl
    {


        public ucPackages()
        {
            InitializeComponent();
            //Label line = new Label();
            //line.BorderStyle = BorderStyle.FixedSingle;
            //line.Text = "";
            //line.AutoSize = false;
            //line.Size = new Size(2, 0);
            //this.Controls.Add(line);
            //line.Dock = DockStyle.Left;

            //Label line2 = new Label();
            //line2.BorderStyle = BorderStyle.FixedSingle;
            //line2.Text = "";
            //line2.AutoSize = false;
            //line2.Size = new Size(2, 0);
            //this.Controls.Add(line2);
            //line2.Dock = DockStyle.Right;

            
            Label line = new Label();
            line.BorderStyle = BorderStyle.FixedSingle;
            line.Text = "";
            line.AutoSize = false;
            line.Size = new Size(0, 2);
            this.Controls.Add(line);
            line.Dock = DockStyle.Top;

        }

        private string packageName;
        private int package_price;
        private string departure;
        private int no_of_days;
        private string destination;
        private Image pic;
        


        #region Getter & Setter For Labels & Picture Box 


        [Category("Custom Props")]
        public string PackageName
        {
            get { return packageName; }
            set { packageName = value; label1.Text = value; }
        }

        [Category("Custom Props")]
        public int Price
        {
            get { return package_price; }
            set
            {
                package_price = value;
                label5.Text = value.ToString();
            }
        }
        [Category("Custom Props")]
        public string Departure
        {
            get { return departure; }
            set { departure = value; label3.Text = value; }
        }
        [Category("Custom Props")]
        public int Days
        {
            
            get { return no_of_days; }
            set
            {
                no_of_days = value;
                label4.Text = value.ToString();
            }
        }
        [Category("Custom Props")]
        public string Destination
        {
            get { return destination; }
            set { destination = value; label2.Text = value; }
        }

        [Category("Custom Props")]
        public Image Pic
        {
            get { return pic; }
            set { pic = value; pictureBox1.Image = value; }
        }

      
       

        #endregion

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void roundButton1_Click(object sender, EventArgs e)
        {
            Admin_TripsClass uc_PackageData = new Admin_TripsClass(
                packageName,
                package_price,
                departure,
                no_of_days,
                destination,
                pic
                );

           ucBookingTicket1.Instance.LoadPackage(uc_PackageData);
           Navigate. ToBookingTicket1();

        }
    }
}
