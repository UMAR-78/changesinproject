﻿
namespace Travel_Agency_Management_System
{
    partial class SignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbox_LName = new System.Windows.Forms.TextBox();
            this.txtbox_FName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtbox_PhoneNumber = new System.Windows.Forms.TextBox();
            this.txtbox_Province = new System.Windows.Forms.TextBox();
            this.txtbox_City = new System.Windows.Forms.TextBox();
            this.txtbox_Country = new System.Windows.Forms.TextBox();
            this.txtbox_EmailAddress = new System.Windows.Forms.TextBox();
            this.txtbox_CurrentAddress = new System.Windows.Forms.TextBox();
            this.txtbox_Pass = new System.Windows.Forms.TextBox();
            this.txtbox_ConfirmPass = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button1.BackColor = System.Drawing.Color.Gold;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(47, 404);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(720, 36);
            this.button1.TabIndex = 14;
            this.button1.Text = "Sign Up";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Black", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(183)))), ((int)(((byte)(3)))));
            this.label3.Location = new System.Drawing.Point(386, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 38);
            this.label3.TabIndex = 13;
            this.label3.Text = "We Travelers";
            // 
            // txtbox_LName
            // 
            this.txtbox_LName.AllowDrop = true;
            this.txtbox_LName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_LName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_LName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_LName.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_LName.Location = new System.Drawing.Point(434, 111);
            this.txtbox_LName.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_LName.Name = "txtbox_LName";
            this.txtbox_LName.Size = new System.Drawing.Size(333, 32);
            this.txtbox_LName.TabIndex = 12;
            this.txtbox_LName.Text = "Last Name";
            this.txtbox_LName.Enter += new System.EventHandler(this.txtbox_LName_Enter);
            this.txtbox_LName.Leave += new System.EventHandler(this.txtbox_LName_Leave);
            // 
            // txtbox_FName
            // 
            this.txtbox_FName.AllowDrop = true;
            this.txtbox_FName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_FName.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_FName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_FName.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_FName.Location = new System.Drawing.Point(47, 111);
            this.txtbox_FName.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_FName.Name = "txtbox_FName";
            this.txtbox_FName.Size = new System.Drawing.Size(333, 32);
            this.txtbox_FName.TabIndex = 9;
            this.txtbox_FName.Text = "First Name";
            this.txtbox_FName.Enter += new System.EventHandler(this.textBox1_Enter);
            this.txtbox_FName.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackgroundImage = global::Travel_Agency_Management_System.Properties.Resources.passport;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Location = new System.Drawing.Point(306, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(65, 60);
            this.panel1.TabIndex = 17;
            // 
            // txtbox_PhoneNumber
            // 
            this.txtbox_PhoneNumber.AllowDrop = true;
            this.txtbox_PhoneNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_PhoneNumber.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_PhoneNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_PhoneNumber.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_PhoneNumber.Location = new System.Drawing.Point(47, 159);
            this.txtbox_PhoneNumber.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_PhoneNumber.Name = "txtbox_PhoneNumber";
            this.txtbox_PhoneNumber.Size = new System.Drawing.Size(333, 32);
            this.txtbox_PhoneNumber.TabIndex = 18;
            this.txtbox_PhoneNumber.Text = "Phone Number";
            this.txtbox_PhoneNumber.Enter += new System.EventHandler(this.txtbox_PhoneNumber_Enter);
            this.txtbox_PhoneNumber.Leave += new System.EventHandler(this.txtbox_PhoneNumber_Leave);
            // 
            // txtbox_Province
            // 
            this.txtbox_Province.AllowDrop = true;
            this.txtbox_Province.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_Province.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_Province.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_Province.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_Province.Location = new System.Drawing.Point(434, 211);
            this.txtbox_Province.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_Province.Name = "txtbox_Province";
            this.txtbox_Province.Size = new System.Drawing.Size(333, 32);
            this.txtbox_Province.TabIndex = 19;
            this.txtbox_Province.Text = "Province";
            this.txtbox_Province.Enter += new System.EventHandler(this.txtbox_Province_Enter);
            this.txtbox_Province.Leave += new System.EventHandler(this.txtbox_Province_Leave);
            // 
            // txtbox_City
            // 
            this.txtbox_City.AllowDrop = true;
            this.txtbox_City.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_City.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_City.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_City.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_City.Location = new System.Drawing.Point(47, 211);
            this.txtbox_City.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_City.Name = "txtbox_City";
            this.txtbox_City.Size = new System.Drawing.Size(333, 32);
            this.txtbox_City.TabIndex = 20;
            this.txtbox_City.Text = "City";
            this.txtbox_City.Enter += new System.EventHandler(this.txtbox_City_Enter);
            this.txtbox_City.Leave += new System.EventHandler(this.txtbox_City_Leave);
            // 
            // txtbox_Country
            // 
            this.txtbox_Country.AllowDrop = true;
            this.txtbox_Country.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_Country.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_Country.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_Country.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_Country.Location = new System.Drawing.Point(434, 159);
            this.txtbox_Country.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_Country.Name = "txtbox_Country";
            this.txtbox_Country.Size = new System.Drawing.Size(333, 32);
            this.txtbox_Country.TabIndex = 21;
            this.txtbox_Country.Text = "Country";
            this.txtbox_Country.Enter += new System.EventHandler(this.txtbox_Country_Enter);
            this.txtbox_Country.Leave += new System.EventHandler(this.txtbox_Country_Leave);
            // 
            // txtbox_EmailAddress
            // 
            this.txtbox_EmailAddress.AllowDrop = true;
            this.txtbox_EmailAddress.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_EmailAddress.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_EmailAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_EmailAddress.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_EmailAddress.Location = new System.Drawing.Point(47, 258);
            this.txtbox_EmailAddress.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_EmailAddress.Name = "txtbox_EmailAddress";
            this.txtbox_EmailAddress.Size = new System.Drawing.Size(720, 32);
            this.txtbox_EmailAddress.TabIndex = 22;
            this.txtbox_EmailAddress.Text = "Email Address";
            this.txtbox_EmailAddress.Enter += new System.EventHandler(this.txtbox_EmailAddress_Enter);
            this.txtbox_EmailAddress.Leave += new System.EventHandler(this.txtbox_EmailAddress_Leave);
            // 
            // txtbox_CurrentAddress
            // 
            this.txtbox_CurrentAddress.AllowDrop = true;
            this.txtbox_CurrentAddress.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_CurrentAddress.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_CurrentAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_CurrentAddress.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_CurrentAddress.Location = new System.Drawing.Point(47, 306);
            this.txtbox_CurrentAddress.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_CurrentAddress.Name = "txtbox_CurrentAddress";
            this.txtbox_CurrentAddress.Size = new System.Drawing.Size(720, 32);
            this.txtbox_CurrentAddress.TabIndex = 23;
            this.txtbox_CurrentAddress.Text = "Current Address";
            this.txtbox_CurrentAddress.Enter += new System.EventHandler(this.txtbox_CurrentAddress_Enter);
            this.txtbox_CurrentAddress.Leave += new System.EventHandler(this.txtbox_CurrentAddress_Leave);
            // 
            // txtbox_Pass
            // 
            this.txtbox_Pass.AllowDrop = true;
            this.txtbox_Pass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_Pass.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_Pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_Pass.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_Pass.Location = new System.Drawing.Point(47, 354);
            this.txtbox_Pass.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_Pass.Name = "txtbox_Pass";
            this.txtbox_Pass.Size = new System.Drawing.Size(333, 32);
            this.txtbox_Pass.TabIndex = 24;
            this.txtbox_Pass.Text = "Password";
            this.txtbox_Pass.Enter += new System.EventHandler(this.txtbox_Pass_Enter);
            this.txtbox_Pass.Leave += new System.EventHandler(this.txtbox_Pass_Leave);
            // 
            // txtbox_ConfirmPass
            // 
            this.txtbox_ConfirmPass.AllowDrop = true;
            this.txtbox_ConfirmPass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtbox_ConfirmPass.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtbox_ConfirmPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbox_ConfirmPass.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
            this.txtbox_ConfirmPass.Location = new System.Drawing.Point(434, 354);
            this.txtbox_ConfirmPass.MinimumSize = new System.Drawing.Size(261, 30);
            this.txtbox_ConfirmPass.Name = "txtbox_ConfirmPass";
            this.txtbox_ConfirmPass.Size = new System.Drawing.Size(333, 32);
            this.txtbox_ConfirmPass.TabIndex = 25;
            this.txtbox_ConfirmPass.Text = "Confirm Password";
            this.txtbox_ConfirmPass.Enter += new System.EventHandler(this.txtbox_ConfirmPass_Enter);
            this.txtbox_ConfirmPass.Leave += new System.EventHandler(this.txtbox_ConfirmPass_Leave);
            // 
            // SignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(48)))), ((int)(((byte)(71)))));
            this.ClientSize = new System.Drawing.Size(816, 497);
            this.Controls.Add(this.txtbox_ConfirmPass);
            this.Controls.Add(this.txtbox_Pass);
            this.Controls.Add(this.txtbox_CurrentAddress);
            this.Controls.Add(this.txtbox_EmailAddress);
            this.Controls.Add(this.txtbox_Country);
            this.Controls.Add(this.txtbox_City);
            this.Controls.Add(this.txtbox_Province);
            this.Controls.Add(this.txtbox_PhoneNumber);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtbox_FName);
            this.Controls.Add(this.txtbox_LName);
            this.Name = "SignUp";
            this.Text = "SignUp";
            this.Load += new System.EventHandler(this.SignUp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbox_LName;
        private System.Windows.Forms.TextBox txtbox_FName;
        private System.Windows.Forms.TextBox txtbox_PhoneNumber;
        private System.Windows.Forms.TextBox txtbox_Province;
        private System.Windows.Forms.TextBox txtbox_City;
        private System.Windows.Forms.TextBox txtbox_Country;
        private System.Windows.Forms.TextBox txtbox_EmailAddress;
        private System.Windows.Forms.TextBox txtbox_CurrentAddress;
        private System.Windows.Forms.TextBox txtbox_Pass;
        private System.Windows.Forms.TextBox txtbox_ConfirmPass;
    }
}