﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Travel_Agency_Management_System
{
    public partial class SignUp : Form
    {
        private static SignUp _instance;
        public static SignUp Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SignUp();
                }
                return _instance;



            }
        }
        private SignUp()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_Enter(object sender, EventArgs e)
        {

            if (txtbox_FName.Text.Equals("First Name"))
            {
                txtbox_FName.Text = "";
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {

            if (txtbox_FName.Text.Equals(""))
            {
                txtbox_FName.Text = "First Name";
            }


        }

        private void txtbox_LName_Enter(object sender, EventArgs e)
        {
            if (txtbox_LName.Text.Equals("Last Name"))
            {
                txtbox_LName.Text = "";
            }
        }

        private void txtbox_LName_Leave(object sender, EventArgs e)
        {
            if (txtbox_LName.Text.Equals(""))
            {
                txtbox_LName.Text = "Last Name";
            }
        }

        private void txtbox_PhoneNumber_Enter(object sender, EventArgs e)
        {
            if (txtbox_PhoneNumber.Text.Equals("Phone Number"))
            {
                txtbox_PhoneNumber.Text = "";
            }
        }

        private void txtbox_PhoneNumber_Leave(object sender, EventArgs e)
        {
            if (txtbox_PhoneNumber.Text.Equals(""))
            {
                txtbox_PhoneNumber.Text = "Phone Number";
            }
        }

        private void txtbox_Country_Enter(object sender, EventArgs e)
        {
            if (txtbox_Country.Text.Equals("Country"))
            {
                txtbox_Country.Text = "";
            }
        }

        private void txtbox_Country_Leave(object sender, EventArgs e)
        {
            if (txtbox_Country.Text.Equals(""))
            {
                txtbox_Country.Text = "Country";
            }
        }

        private void txtbox_City_Enter(object sender, EventArgs e)
        {
            if (txtbox_City.Text.Equals("City"))
            {
                txtbox_City.Text = "";
            }
        }

        private void txtbox_City_Leave(object sender, EventArgs e)
        {
            if (txtbox_City.Text.Equals(""))
            {
                txtbox_City.Text = "City";
            }
        }

        private void txtbox_Province_Enter(object sender, EventArgs e)
        {
            if (txtbox_Province.Text.Equals("Province"))
            {
                txtbox_Province.Text = "";
            }
        }

        private void txtbox_Province_Leave(object sender, EventArgs e)
        {
            if (txtbox_Province.Text.Equals(""))
            {
                txtbox_Province.Text = "Province";
            }
        }

        private void txtbox_EmailAddress_Enter(object sender, EventArgs e)
        {
            if (txtbox_EmailAddress.Text.Equals("Email Address"))
            {
                txtbox_EmailAddress.Text = "";
            }
        }

        private void txtbox_EmailAddress_Leave(object sender, EventArgs e)
        {
            if (txtbox_EmailAddress.Text.Equals(""))
            {
                txtbox_EmailAddress.Text = "Email Address";
            }
        }

        private void txtbox_CurrentAddress_Enter(object sender, EventArgs e)
        {
            if (txtbox_CurrentAddress.Text.Equals("Current Address"))
            {
                txtbox_CurrentAddress.Text = "";
            }
        }

        private void txtbox_CurrentAddress_Leave(object sender, EventArgs e)
        {
            if (txtbox_CurrentAddress.Text.Equals(""))
            {
                txtbox_CurrentAddress.Text = "Current Address";
            }
        }

        private void txtbox_Pass_Enter(object sender, EventArgs e)
        {
            if (txtbox_Pass.Text.Equals("Password"))
            {
                txtbox_Pass.Text = "";
            }
        }

        private void txtbox_Pass_Leave(object sender, EventArgs e)
        {
            if (txtbox_Pass.Text.Equals(""))
            {
                txtbox_Pass.Text = "Password";
            }
        }

        private void txtbox_ConfirmPass_Enter(object sender, EventArgs e)
        {
            if (txtbox_ConfirmPass.Text.Equals("Confirm Password"))
            {
                txtbox_ConfirmPass.Text = "";
            }
        }

        private void txtbox_ConfirmPass_Leave(object sender, EventArgs e)
        {
            if (txtbox_ConfirmPass.Text.Equals(""))
            {
                txtbox_ConfirmPass.Text = "Confirm Password";
            }
        }
    }
}



